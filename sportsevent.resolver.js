/*
    User resolvers
*/
// resolver functions

const addHorseRaceEvent = require("./resolvers/add_horse_race_event");
const addGreyhoundRaceEvent = require("./resolvers/add_greyhound_race_event");
const getGreyhoundRaces = require("./resolvers/get_greyhound_races");

module.exports = {
  Query: {
    getGreyhoundRaces,
  },
  Mutation: {
    addHorseRaceEvent,
    addGreyhoundRaceEvent,
  },
};
