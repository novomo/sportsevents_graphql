const mysqlx = require("@mysql/xdevapi");
const queryBuilder = require("../../../data/mysql/query_builder");
const { toTimestamp } = require("../../../node_normalization/numbers");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
module.exports = async (_, { inputHorseRace }, { currentUser }) => {
  try {
    if (!currentUser) {
      throw new Error("Not authenicated!");
    }
    console.log(inputHorseRace);

    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE);
    const horseRaceColl = await mysqlDb.getCollection("horse_races");
    let horseRace = await horseRaceColl
      .find("url like :url")
      .bind("url", inputHorseRace.url)
      .execute();

    horseRace = horseRace.fetchOne();
    console.log(horseRace);
    if (horseRace) {
      return horseRace._id;
    }

    // Track

    const courseTable = await mysqlDb.getTable("horse_tracks");
    console.log(courseTable);
    let trackRow = await courseTable
      .select()
      .where("name = :name")
      .bind("name", inputHorseRace.course.name)
      .execute();

    trackRow = trackRow.fetchOne();
    let horseTrackID;

    console.log("trackRow");
    console.log(trackRow);
    if (!trackRow) {
      const track = {
        name: inputHorseRace.course.name,
      };
      console.log(track);

      horseTrackID = await queryBuilder(
        session,
        "insert",
        track,
        "horse_tracks"
      );
    } else {
      horseTrackID = trackRow[0];
    }

    let runners = [];

    for (let i = 0; i < inputHorseRace.runners.length; i++) {
      // Jockey
      const runner = inputHorseRace.runners[i];

      const jockeyTable = await mysqlDb.getTable("jockeys");

      let jockeyRow = await jockeyTable
        .select()
        .where("url = :url")
        .bind("url", runner.jockey.url)
        .execute();

      jockeyRow = jockeyRow.fetchOne();
      console.log("jockeyRow");
      console.log(jockeyRow);
      let jockeyID;
      if (!jockeyRow) {
        const jockey = {
          name: runner.jockey.name,
          url: runner.jockey.url,
        };

        jockeyID = await queryBuilder(session, "insert", jockey, "jockeys");
      } else {
        jockeyID = jockeyRow[0];
      }

      const horseTable = await mysqlDb.getTable("horses");

      let horseRow = await horseTable
        .select()
        .where("url = :url")
        .bind("url", runner.horse.url)
        .execute();

      horseRow = horseRow.fetchOne();
      let horseID;
      if (!horseRow) {
        const horse = {
          url: runner.horse.url,
          age: runner.horse.age,
          weight: runner.horse.weight,
          form: runner.horse.form,
          runningPosition: runner.horse.runningPosition,
          name: runner.horse.name,
        };

        await queryBuilder(session, "insert", horse, "horses");
      } else {
        horseID = horseRow[0];
      }

      runners.push({
        horseID: horseID,
        jockeyID: jockeyID,
        gate: runner.gate,
        equipment: runner.equipment,
      });
    }

    // Horses

    let horseRaceID = await horseRaceColl
      .add({
        url: inputHorseRace.url,
        starttime: inputHorseRace.starttime,
        lastUpdated: toTimestamp(new Date()),
        results: "",
        runners: runners,
        course: {
          courseID: horseTrackID,
          distance: inputHorseRace.course.distance,
          raceType: inputHorseRace.course.raceType,
          courseType: inputHorseRace.course.courseType,
          ground: inputHorseRace.course.ground,
        },
      })
      .execute();

    horseRaceID = horseRaceID.getGeneratedIds()[0];
    await session.close();
    return horseRaceID;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Adding Horse Race",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
