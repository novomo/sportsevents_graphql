const mysqlx = require("@mysql/xdevapi");
const queryBuilder = require("../../../data/mysql/query_builder");
const { toTimestamp } = require("../../../node_normalization/numbers");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");
module.exports = async (_, { inputGreyhoundRace }, { currentUser }) => {
  try {
    if (!currentUser) {
      throw new Error("Not authenicated!");
    }
    console.log(inputGreyhoundRace);

    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE);
    const greyhoundRaceColl = await mysqlDb.getCollection("greyhound_races");
    let greyhoundRace = await greyhoundRaceColl
      .find("url like :url")
      .bind("url", inputGreyhoundRace.url)
      .execute();

    greyhoundRace = greyhoundRace.fetchOne();
    console.log(greyhoundRace);
    if (greyhoundRace) {
      return greyhoundRace._id;
    }

    // Track

    const trackTable = await mysqlDb.getTable("greyhound_tracks");
    console.log(trackTable);
    let trackRow = await trackTable
      .select()
      .where("name = :name")
      .bind("name", inputGreyhoundRace.track.name)
      .execute();

    trackRow = trackRow.fetchOne();
    let greyhoundTrackID;

    console.log("trackRow");
    console.log(trackRow);
    if (!trackRow) {
      const track = {
        name: inputGreyhoundRace.track.name,
      };
      console.log(track);

      greyhoundTrackID = await queryBuilder(
        session,
        "insert",
        track,
        "greyhound_tracks"
      );
    } else {
      greyhoundTrackID = trackRow[0];
    }

    let runners = [];

    for (let i = 0; i < inputGreyhoundRace.runners.length; i++) {
      // Jockey
      const runner = inputGreyhoundRace.runners[i];

      const trainerTable = await mysqlDb.getTable("trainers");

      let trainerRow = await trainerTable
        .select()
        .where("url = :url")
        .bind("url", runner.trainer.url)
        .execute();

      trainerRow = trainerRow.fetchOne();
      console.log("trainerRow");
      console.log(trainerRow);
      let trainerID;
      if (!trainerRow) {
        const trainer = {
          name: runner.trainer.name,
          url: runner.trainer.url,
        };

        trainerID = await queryBuilder(session, "insert", trainer, "trainers");
      } else {
        trainerID = trainerRow[0];
      }

      const greyhoundTable = await mysqlDb.getTable("greyhounds");

      let greyhoundRow = await greyhoundTable
        .select()
        .where("url = :url")
        .bind("url", runner.greyhound.url)
        .execute();

      greyhoundRow = greyhoundRow.fetchOne();
      let greyhoundID;
      if (!greyhoundRow) {
        const greyhound = {
          url: runner.greyhound.url,
          form: runner.greyhound.form,
          name: runner.greyhound.name,
        };

        greyhoundID = await queryBuilder(
          session,
          "insert",
          greyhound,
          "greyhounds"
        );
      } else {
        greyhoundID = greyhoundRow[0];
      }

      runners.push({
        greyhoundID: greyhoundID,
        trainerID: trainerID,
        trap: runner.trap,
        sex: runner.sex,
        mstrSpd: runner.mstrSpd,
        age: runner.age,
        sectSpd: runner.sectSpd,
      });
    }

    // Horses

    let greyhoundRaceID = await greyhoundRaceColl
      .add({
        url: inputGreyhoundRace.url,
        starttime: inputGreyhoundRace.starttime,
        lastUpdated: toTimestamp(new Date()),
        results: "",
        marketID: 0,
        runners: runners,
        track: {
          trackID: greyhoundTrackID,
          distance: inputGreyhoundRace.track.distance,
          raceType: inputGreyhoundRace.track.raceType,
        },
      })
      .execute();

    greyhoundRaceID = greyhoundRaceID.getGeneratedIds()[0];
    await session.close();
    return greyhoundRaceID;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Adding Greyhound Race",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
