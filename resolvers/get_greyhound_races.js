const compileGreyhoundRace = require("../constants/greyhoundrace_compiler");
const { findByValueOfObject } = require("../../../node_normalization/filters");
const { formatDate } = require("../../../node_normalization/dates");
const mysqlx = require("@mysql/xdevapi");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
const { pubsub } = require("../../../constants/pubsub");
const {
  addDataToSheet,
} = require("../../../google_sheet_actions/add_to_sheet_queue");
let { client, reconnect } = require("../../../data/mysql/sql_connection");

module.exports = async (
  _,
  { query },
  {
    currentUser,
    greyhoundDataLoader,
    trainerDataLoader,
    greyhoundTrackDataLoader,
    greyhoundRaceDataLoader,
  }
) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }

  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  try {
    const greyhoundRaceColl = await mysqlDb.getCollection("greyhound_races");
    console.log(greyhoundRaceColl);
    let greyhoundRaces = await greyhoundRaceColl.find(query).execute();
    console.log(greyhoundRaces);
    greyhoundRaces = greyhoundRaces.fetchAll();
    console.log(greyhoundRaces);

    let finalRaces = await Promise.all(
      greyhoundRaces.map(async (race) => {
        return await compileGreyhoundRace(
          race._id,
          race,
          greyhoundDataLoader,
          trainerDataLoader,
          greyhoundTrackDataLoader,
          greyhoundRaceDataLoader
        );
      })
    );
    console.log(finalRaces);
    await session.close();
    return finalRaces;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "etting Greyhound Race",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
