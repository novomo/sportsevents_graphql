const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getCompetitionById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const competitionTable = await mysqlDb.getTable("competitions");
  let competition = await competitionTable
    .find("_id = :CompetitionID")
    .bind("CompetitionID", id)
    .execute();

  competition = competition.fetchOne();
  await session.close();
  return competition;
};

module.exports.getCompetitionByIds = async (ids) => {
  return ids.map((id) => getCompetitionById(id));
};
