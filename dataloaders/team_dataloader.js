const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getTeamById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const teamTable = await mysqlDb.getTable("teams");
  let team = await teamTable.find("_id = :TeamID").bind("TeamID", id).execute();

  team = team.fetchOne();
  await session.close();
  return team;
};

module.exports.getTeamByIds = async (ids) => {
  return ids.map((id) => getTeamById(id));
};
