const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getGreyhoundTrackById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  try {
    const greyhoundTrackTable = await mysqlDb.getTable("greyhound_tracks");
    let greyhoundTrack = await greyhoundTrackTable
      .select()
      .where("trackID = :greyhoundTrackID")
      .bind("greyhoundTrackID", id)
      .execute();

    greyhoundTrack = greyhoundTrack.fetchOne();
    //console.log(greyhoundTrack);
    await session.close();
    return greyhoundTrack;
  } catch (err) {
    console.log(err);
  }
};

module.exports.getGreyhoundTrackByIds = async (ids) => {
  return ids.map((id) => getGreyhoundTrackById(id));
};
