const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getJockeyById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const jockeyTable = await mysqlDb.getTable("jockeys");
  let jockey = await jockeyTable
    .select()
    .where("jockeyID = :jockeyID")
    .bind("jockeyID", id)
    .execute();

  jockey = jockey.fetchOne();
  await session.close();
  return jockey;
};

module.exports.getJockeyByIds = async (ids) => {
  return ids.map((id) => getJockeyById(id));
};
