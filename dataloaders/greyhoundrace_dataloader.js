const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getGreyhoundRaceById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const greyhoundRaceColl = await mysqlDb.getCollection("greyhound_races");
  let greyhoundRace = await greyhoundRaceColl
    .find("_id = :greyhoundRaceID")
    .bind("greyhoundRaceID", id)
    .execute();

  greyhoundRace = greyhoundRace.fetchOne();
  await session.close();
  return greyhoundRace;
};

module.exports.getGreyhoundRaceByIds = async (ids) => {
  return ids.map((id) => getGreyhoundRaceById(id));
};
