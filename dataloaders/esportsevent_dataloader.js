const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getEsportsEventById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const esportsEventColl = await mysqlDb.getCollection("esports_events");
  let esportsEvent = await esportsEventColl
    .find("_id = :esportsEventID")
    .bind("esportsEventID", id)
    .execute();

  esportsEvent = esportsEvent.fetchOne();
  await session.close();
  return esportsEvent;
};

module.exports.getEsportsEventByIds = async (ids) => {
  return ids.map((id) => getEsportsEventById(id));
};
