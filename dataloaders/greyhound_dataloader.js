const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getGreyhoundById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  try {
    const greyhoundTable = await mysqlDb.getTable("greyhounds");
    let greyhound = await greyhoundTable
      .select()
      .where("greyhoundID = :greyhoundID")
      .bind("greyhoundID", id)
      .execute();

    greyhound = greyhound.fetchOne();
    //console.log(greyhound);
    await session.close();
    return greyhound;
  } catch (err) {
    console.log(err);
  }
};

module.exports.getGreyhoundByIds = async (ids) => {
  return ids.map((id) => getGreyhoundById(id));
};
