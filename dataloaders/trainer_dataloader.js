const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getTrainerById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  try {
    const trainerTable = await mysqlDb.getTable("trainers");
    let trainer = await trainerTable
      .select()
      .where("trainerID = :trainerID")
      .bind("trainerID", id)
      .execute();

    trainer = trainer.fetchOne();
    //console.log(trainer);
    await session.close();
    return trainer;
  } catch (err) {
    console.log(err);
  }
};

module.exports.getTrainerByIds = async (ids) => {
  return ids.map((id) => getTrainerById(id));
};
