const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getCourseById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const courseTable = await mysqlDb.getTable("courses");
  let course = await courseTable
    .select()
    .where("courseID = :courseID")
    .bind("courseID", id)
    .execute();

  course = course.fetchOne();
  await session.close();
  return course;
};

module.exports.getCourseByIds = async (ids) => {
  return ids.map((id) => getCourseById(id));
};
