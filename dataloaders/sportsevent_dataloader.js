const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getSportsEventById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const sportsEventColl = await mysqlDb.getCollection("sports_events");
  let sportsEvent = await sportsEventColl
    .find("_id = :sportsEventID")
    .bind("sportsEventID", id)
    .execute();

  sportsEvent = sportsEvent.fetchOne();
  await session.close();
  return sportsEvent;
};

module.exports.getSportsEventByIds = async (ids) => {
  return ids.map((id) => getSportsEventById(id));
};
