const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getHorseById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const horseTable = await mysqlDb.getTable("horses");
  let horse = await horseTable
    .select()
    .where("horseID = :horseID")
    .bind("horseID", id)
    .execute();

  horse = horse.fetchOne();
  await session.close();
  return horse;
};

module.exports.getHorseByIds = async (ids) => {
  return ids.map((id) => getHorseById(id));
};
