const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const getHorseRaceById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const horseRaceColl = await mysqlDb.getCollection("horse_races");
  let horseRace = await horseRaceColl
    .find("_id = :horseRaceID")
    .bind("horseRaceID", id)
    .execute();

  horseRace = horseRace.fetchOne();
  await session.close();
  return horseRace;
};

module.exports.getHorseRaceByIds = async (ids) => {
  return ids.map((id) => getHorseRaceById(id));
};
