const { connect } = require("../../../data/mysql/sql_connection");

module.exports = async (
  mysqlDb,
  sportsEventId,
  sportsEvent,
  teamDataLoader,
  competitionDataLoader,
  eSportEventDataLoader
) => {
  let s, t;
  if (!sportsEvent) {
    s = await eSportEventDataLoader.load(sportsEventId);
  } else {
    s = sportsEvent;
  }

  return {
    ...s,
    homeTeam: await teamDataLoader.load(s.homeTeamID),
    awayTeam: await teamDataLoader.load(s.homeTeamID),
    competition: await competitionDataLoader.load(s.competitionID),
  };
};
