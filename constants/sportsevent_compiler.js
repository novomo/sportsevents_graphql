const { connect } = require("../../../data/mysql/sql_connection");

module.exports = async (
  mysqlDb,
  sportsEventId,
  sportsEvent,
  teamDataLoader,
  competitionDataLoader,
  sportEventDataLoader
) => {
  let s, t;
  if (!sportsEvent) {
    s = await sportEventDataLoader.load(sportsEventId);
  } else {
    s = sportsEvent;
  }

  return {
    ...s,
    homeTeam: await teamDataLoader.load(s.homeTeamID),
    awayTeam: await teamDataLoader.load(s.homeTeamID),
    competition: await competitionDataLoader.load(s.competitionID),
  };
};
