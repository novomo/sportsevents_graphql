const { connect } = require("../../../data/mysql/sql_connection");

module.exports = async (
  greyhoundId,
  greyhoundRace,
  greyhoundDataLoader,
  trainerDataLoader,
  greyhoundTrackDataLoader,
  greyhoundRaceDataLoader
) => {
  let g, t;
  if (!greyhoundRace) {
    g = await greyhoundRaceDataLoader.load(greyhoundId);
  } else {
    g = greyhoundRace;
  }
  let track = await greyhoundTrackDataLoader.load(g.track.trackID);
  return {
    ...g,
    runners: await Promise.all(
      g.runners.map(async (runner) => {
        return {
          ...runner,
          greyhound: await greyhoundDataLoader.load(runner.greyhoundID),
          trainer: await trainerDataLoader.load(runner.trainerID),
        };
      })
    ),
    track: {
      name: track[1],
      starttime: track[2],
      ...g.track,
    },
  };
};
