const { connect } = require("../../../data/mysql/sql_connection");

module.exports = async (
  mysqlDb,
  horseRaceId,
  horseRace,
  horseDataLoader,
  jockeyDataLoader,
  horseRaceTrackDataLoader,
  horseRaceDataloader
) => {
  let h, t;
  if (!horseRace) {
    h = await horseRaceDataLoader.load(horseRaceId);
  } else {
    h = horseRace;
  }

  return {
    ...h,
    runners: await Promise.all(
      h.runnner.map(async (runner) => {
        return {
          ...runner,
          horse: await horseDataLoader.load(runner.horseID),
          jockey: await jockeyDataLoader.load(runner.jockeyID),
        };
      })
    ),
    course: await horseRaceTrackDataLoader.load(h.courseID),
  };
};
